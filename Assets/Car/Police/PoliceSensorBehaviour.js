#pragma strict

class PoliceSensorBehaviour extends SensorBehaviour
{
	
	enum EvasiveAction
	{
		TurnLeft,
		TurnRight,
		Break
	}

	var mainCollider : Collider;
	
	private var action : EvasiveAction;
	
	private var leftPoint : Vector3;
	private var rightPoint : Vector3;
	/**
	 * Determine evasive action at the beginning (turn left/right or break)
	 */
	function startBehaviour(other : Collider)
	{
		if(mainCollider == null) {
			mainCollider = transform.Find("Collider_Bottom").collider;
		}
		//linecast to the left first (we're driving on the right, so we expect more space on the left)
		leftPoint = transform.TransformPoint(Vector3(-mainCollider.bounds.extents.z, 0, 0));
		rightPoint = transform.TransformPoint(Vector3(mainCollider.bounds.extents.z, 0, 0));
		
		//if clear steer to the left, else check right
		var layerMask = 1 << 9;
		layerMask = ~layerMask;
		
		if(!Physics.Linecast(transform.position, leftPoint, layerMask)) {
			action = EvasiveAction.TurnLeft;
		}
		else if(!Physics.Linecast(transform.position, rightPoint, layerMask)) {
			action = EvasiveAction.TurnRight;
		}
		else {
			action = EvasiveAction.Break;
		}
	}
	
	function performBehaviour(other : Collider)
	{
		switch(action)
		{
			case EvasiveAction.TurnLeft:
				Debug.DrawLine(transform.position, leftPoint, Color.magenta);
				carController.turnLeft(1);
				break;
				
			case EvasiveAction.TurnRight:
				Debug.DrawLine(transform.position, rightPoint, Color.cyan);
				carController.turnRight(1);
				break;
				
			case EvasiveAction.Break:
				if(rigidbody.velocity.magnitude >= 0.1) {
					carController.reverse(1);
				} else {
					carController.stop(1);
				}
		}
	}
}