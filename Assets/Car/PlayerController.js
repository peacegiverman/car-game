#pragma strict

var acceleration : float;

function Start () {

}

function FixedUpdate() 
{
	var xForce = Input.GetAxis('Horizontal') * acceleration;
	var zForce = Input.GetAxis('Vertical') * acceleration; 
	rigidbody.AddForce(xForce, 0, zForce);
}