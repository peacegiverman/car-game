#pragma strict

class RegularSensorBehaviour extends SensorBehaviour
{
	var regularDriving : RegularDriving;
	
	function startBehaviour(other : Collider)
	{
		if(other.CompareTag("Crossroad"))
		{
			var crossroad = other.transform.GetComponent(Crossroad);
			var possibleRotations = Array(crossroad.possibleRotations);
			for(var r : float in possibleRotations)
			{
				r += other.transform.eulerAngles.y;
				r = r % 360;
			}
			regularDriving.atCrossroad(possibleRotations);
		}
		else
		{
			regularDriving.stop();
		}
	}
	
	function stopBehaviour(other : Collider)
	{
		if(!other.CompareTag("Crossroad"))
		{
			regularDriving.go();
		}
	}
}