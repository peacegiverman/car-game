#pragma strict

private var throttle : float = 0;
private var steer : float = 0;

//Lower values have higher priority
private var currentThrottlePriority : int = 1000;
private var currentSteerPriority : int = 1000;
private var lowestPriority : int = 1000;

function forward(priority : int)
{
	if(priority <= currentThrottlePriority) {
		throttle = 1;
		currentThrottlePriority = priority;
	}
}

function forward() {
	forward(lowestPriority);
}

function reverse(priority : int)
{
	if(priority <= currentThrottlePriority) {
		throttle = -1;
		currentThrottlePriority = priority;
	}
}

function reverse() {
	reverse(lowestPriority);
}

function turnLeft(priority : int)
{
	if(priority <= currentSteerPriority) {
		steer = -1;
		currentSteerPriority = priority;
	}
}

function turnLeft() {
	turnLeft(lowestPriority);
}

function turnRight(priority : int)
{
	if(priority <= currentSteerPriority) {
		steer = 1;
		currentSteerPriority = priority;
	}
}

function turnRight() {
	turnRight(lowestPriority);
}

function stop(priority : int)
{
	if(priority <= currentThrottlePriority && priority <= currentSteerPriority)
	{
		throttle = 0;
		steer = 0;
		currentThrottlePriority = priority;
		currentSteerPriority = priority;
	}
}

function stop() {
	stop(lowestPriority);
}

function addThrottle(throttleIncrement : float, priority : int)
{
	if(priority <= currentThrottlePriority) {
		throttle += throttleIncrement;
		currentThrottlePriority = priority;
	}
}

function addThrottle(throttleIncrement : float) {
	addThrottle(throttleIncrement, lowestPriority);
}

function addSteer(steerIncrement : float, priority : int)
{
	if(priority <= currentSteerPriority)
	{
		steer += steerIncrement;
		steer = Mathf.Min(steer, 1);
		steer = Mathf.Max(steer, -1);
		
		currentSteerPriority = priority;
	}
}

function addSteer(steerIncrement : float) {
	addSteer(steerIncrement, lowestPriority);
}

function resetThrottle(priority : int)
{
	if(priority <= currentThrottlePriority) {
		throttle = 0;
		currentThrottlePriority = priority;
	}
}

function resetThrottle() {
	resetThrottle(lowestPriority);
}

function resetSteer(priority : int)
{
	if(priority <= currentThrottlePriority) {
		steer = 0;
		currentSteerPriority = priority;
	}
}

function resetSteer() {
	resetSteer(lowestPriority);
}

function getThrottle()
{
	var t = throttle;
	throttle = 0;
	
	currentThrottlePriority = lowestPriority;
	
	return t;
}

function getSteer()
{
	var s = steer;
	steer = 0;
	
	currentSteerPriority = lowestPriority;
	
	return s;
}