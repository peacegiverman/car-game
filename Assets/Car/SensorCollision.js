#pragma strict

var sensorBehaviour : SensorBehaviour;

var appliedThrottle : float = 0.0;
var appliedSteer : float = 0.0;

private var nearCar : boolean = false;


function OnTriggerEnter(other : Collider)
{
	if(!other.CompareTag("Terrain")) {
		if(sensorBehaviour != null) {
			sensorBehaviour.startBehaviour(other);
		}
	}
}

function OnTriggerStay(other : Collider)
{
	//Should call SensorBehaviourEnter() from SensorBehaviour script
	if(other.CompareTag("sensor")) {
		//carController.stop();
	}
	//else if(!other.CompareTag("Terrain")){
	if(!other.CompareTag("Terrain"))
	{
		if(sensorBehaviour != null) {
			sensorBehaviour.performBehaviour(other);
		}
//		if(appliedThrottle != 0)
//		{
//			carController.resetThrottle(1); // override throttle
//			carController.addThrottle(appliedThrottle, 1);
//		}
//		if(appliedSteer != 0)
//		{
//			carController.resetSteer(1); //override steer
//			carController.addSteer(appliedSteer, 1);
//		}
	}
}

function OnTriggerExit(other :Collider)
{
	if(!other.CompareTag("Terrain")) {
		if(sensorBehaviour != null) {
			sensorBehaviour.stopBehaviour(other);
		}
	}
	//carController.go();
}