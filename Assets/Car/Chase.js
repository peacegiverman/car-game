#pragma strict

var target : Transform;
var threshold : float = 1;

private var carController : CarController;
private var chase : boolean = false;

private var targetNodeList : Array;
private var targetNode : Vector3;
private var lastTargetPosition : Vector3;

var nodeRange: float = 3.0;

function Start()
{
	carController = GetComponent(CarController);

	targetNodeList = new Array();	
	targetNode = target.position;
	lastTargetPosition = target.position;
}

private var steerIncrement : float = 0.1;

function FixedUpdate () 
{
	if(!chase) {
		return;
	}
	
	/*
	 The idea is to raycast from the police car to the player until we hit an object in between.
	 We add that position to a list, and raycast between that position and the player, until
	 the police car reach that position. Thus we keep a list of nodes through which the cop must go
	 */
	//seekTarget();
	var layerMask = 1 << 8;
	var lineStart = transform.position;
	//TODO: Possibility: always check if there is direct visibility from cop to player and
	//cancel all nodes if there is.
	if(targetNodeList.length > 0) {
		lineStart = targetNodeList[targetNodeList.length - 1]; //last node
	}
	Debug.DrawLine(lineStart, target.position, Color.blue, 5);
	if(Physics.Linecast(lineStart, target.position, layerMask)) //(s/transform.position/lastNode/)
	{
		targetNodeList.Add(lastTargetPosition);
	}
	
	//Are we close to the first target node?
	if(targetNodeList.length > 0 && Vector3.Distance(transform.position, targetNodeList[0]) < nodeRange) { 
			targetNodeList.RemoveAt(0); //remove first node
	}
	
	if(targetNodeList.length > 0) {
		targetNode = targetNodeList[0];
	}
	else {
		targetNode = target.position;
	}
	
	lastTargetPosition = target.parent.position;
	
	var lastNode = transform.position;
	for(var node : Vector3 in targetNodeList)
	{
		Debug.DrawLine(lastNode, node, Color.red, 5);
		lastNode = node;
	}

	var targetLocal = transform.InverseTransformPoint(targetNode);
	
	if(targetLocal.x < -threshold) {
		if(steerIncrement > 0) {
			steerIncrement = 0;
		}
		steerIncrement -= 0.1;
		steerIncrement = Mathf.Max(steerIncrement, -1);
	
		carController.addSteer(steerIncrement);
	}
	else if(targetLocal.x > threshold) {
		if(steerIncrement < 0) {
			steerIncrement = 0;
		}
		
		steerIncrement += 0.1;
		steerIncrement = Mathf.Min(steerIncrement, 1);
		
		carController.addSteer(steerIncrement);
	}
	else {
		carController.resetSteer();
	}
	
	if(targetLocal.z > threshold) {
		carController.forward();
	} else if(targetLocal.z < -threshold) {
		carController.reverse();
		carController.addSteer(-2*steerIncrement);
	}

}

function StartChase()
{
	chase = true;
}

function OnDrawGizmos()
{
	if(targetNodeList != null && targetNodeList.length > 0) {
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(targetNodeList[0], nodeRange);
	}
}