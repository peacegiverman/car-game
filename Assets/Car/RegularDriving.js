#pragma strict

var carController : CarController;

var threshold : float = 0.1;

private var isBreaking = false;
private var isTurning = false;

private var newRotation : float = 0;

private var currentThrottle : float = 0.5;
private var currentSteer : float = 0.0;

function Start()
{
	carController = transform.GetComponent(CarController);
}

function Update () {
	
	if(isBreaking)
	{
		if(rigidbody.velocity.magnitude > 0.1) {
			carController.addThrottle(-0.5);
		} else {
			carController.stop();
		}
		currentThrottle = 0.0;
	}
	
	if(isTurning)
	{
		if((Mathf.Abs(Mathf.DeltaAngle(transform.eulerAngles.y, newRotation)) < threshold))
		{
			isTurning = false;
			currentSteer = 0.0;
			transform.eulerAngles = Vector3(transform.eulerAngles.x, newRotation, transform.eulerAngles.z);
			currentThrottle = 0.5;
		} 
		else {
			carController.addSteer(currentSteer);
			currentThrottle = 0.0;
		}
	}
		
	carController.addThrottle(currentThrottle);
}

function stop()
{
	isBreaking = true;
}

function go()
{
	isBreaking = false;
}

function atCrossroad(possibleRotations : Array)
{
	//check if the car can keep going in the same direction
	for(var i : int = 0; i < possibleRotations.length; i++)
	{
		//FIXME: should be a range
		if(possibleRotations[i] == transform.eulerAngles.y) {
			//greater chance to keep going forward
			if(Random.Range(0, 10) <= 6 ) {
				return;
			} else {
				possibleRotations.RemoveAt(i);
			}
		}
		//no U-turns
		else if(Mathf.DeltaAngle(possibleRotations[i], transform.eulerAngles.y + 180) < threshold) {
			possibleRotations.RemoveAt(i);
		}
	}
		
	for(r in possibleRotations) {
		Debug.Log("Angle: "+possibleRotations);
	}
	newRotation = possibleRotations[Random.Range(0, possibleRotations.length)];
	
	currentSteer = (((newRotation - transform.eulerAngles.y) + 360f) % 360f) > 180.0f ? -1 : 1;
	isTurning = true;
}