#pragma strict

var carController : CarController;

private var isStopped = false;

function Start()
{
	carController = transform.GetComponent(CarController);
}
function Update () {
	
	if(isStopped) {
		if(rigidbody.velocity.magnitude > 0.1) {
			carController.reverse();
		} else {
			carController.stop();
		}
		return;
	}
		
	carController.forward();
	if(Random.Range(0, 1)) {
		carController.turnLeft();
	}
}

function stop()
{
	isStopped = true;
}

function go()
{
	isStopped = false;
}