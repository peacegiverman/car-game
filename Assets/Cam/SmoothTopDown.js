#pragma strict

var target: GameObject;
var targetDistance: float = 50;
var lowAngle:float = 30;
var highAngle:float = 10;
private var riseSpeed: float = 1.0f;

function Start () 
{

}

function Update () 
{

}

function inRange(val:float, min:float, max:float)
{
	return (val - min) / (max - min);
}

function FixedUpdate()
{
	var tt = target.transform;
	var deltaPos = transform.position - tt.position;
	var angleToUp:float = Vector3.Angle(tt.up, deltaPos);
	var riseX:float = inRange(angleToUp, highAngle, lowAngle);
	transform.position.y += riseX * riseSpeed;

	transform.position = Vector3.MoveTowards(
		transform.position, tt.position, deltaPos.magnitude - targetDistance);
	transform.LookAt(target.transform);
}