#pragma strict

var suspicionLevel : int = 0;
var maxSuspicionLevel : int = 10;

var cash : int = 0; //score

var suspicionListeners : Array;

function Start () 
{
	suspicionListeners = new Array();
}

function Update() 
{

}

function raiseSuspicion()
{
	raiseSuspicion(1);
}

function raiseSuspicion(inc : int)
{
	suspicionLevel += inc;
	for(var o:GameObject in suspicionListeners)
	{
		o.SendMessage("SuspicionRaised", suspicionLevel);
	}
}

function RegisterSuspicionListener(o : GameObject)
{
	suspicionListeners.Push(o);
	Debug.Log("lisnin");
}

function UnregisterSuspicionListener(o : GameObject)
{
	suspicionListeners.Remove(o);
}

function OnCollisionEnter()
{
	raiseSuspicion();
}