#pragma strict

private var player : Transform;
private var playerState : PlayerState;

function Start () {
	player = GameObject.FindGameObjectWithTag("Player").transform;
	playerState = player.GetComponent(PlayerState);
}

function Update () {
}

function OnTriggerEnter(other : Collider)
{
	if(other.CompareTag("Player Collider"))
	{
		Debug.Log(playerState);
		if(playerState.suspicionLevel > 3) {
			transform.parent.SendMessage("StartChase");
		}
		else {
			player.SendMessage("RegisterSuspicionListener", gameObject);
		}
	}
}

function OnTriggerExit(other : Collider)
{
	if(other.CompareTag("Player Collider")) {
		player.SendMessage("UnregisterSuspicionListener", gameObject);
	}
}

function SuspicionRaised()
{
	transform.parent.SendMessage("StartChase");
}